## sigAnnotateBoxplots
Annotates ggplot2 boxplots with significance clipses and text.


#### Installation
library(devtools)  
install_bitbucket("sigannotateboxplots","karel_fiser")  

#### List of functions :-) :
- sigAnnotateBoxplots 

#### Demos :
- demo(demo_diamonds) 
- demo(demo_mtcars)  

#### Notes
roxygenise to get documentation:  
library(roxygen2)  
roxygenise() 
  
help(sigAnnotateBoxplots)  
  
#### TODO  
- handle two variable faceting
- more examples
- demo_diamonds.R: are the stats ok???
- mc1, mc2 magic constants should have better defaults. What is the ggplot2 logic to axis distances?